# Common functionality for LHCb Analysis Productions packages

 [![pipeline status](https://gitlab.cern.ch/lhcb-dpa/analysis-productions/lbapcommon/badges/master/pipeline.svg)](https://gitlab.cern.ch/lhcb-dpa/analysis-productions/lbapcommon/-/commits/master)
 [![coverage report](https://gitlab.cern.ch/lhcb-dpa/analysis-productions/lbapcommon/badges/master/coverage.svg)](https://gitlab.cern.ch/lhcb-dpa/analysis-productions/lbapcommon/-/commits/master)

This repository contains the implementation of `LbAPCommon`, a Python package for shareing functionality between:

* [LbAnalysisProductions](https://gitlab.cern.ch/lhcb-dpa/analysis-productions/LbAnalysisProductions/)
* [LbAPLocal](https://gitlab.cern.ch/lhcb-dpa/analysis-productions/LbAPLocal/)

This package is currently unstable to allow it to be adapted for the needs of its dependent packages.
